 var vendorApp = angular.module("vendor-app",["ngRoute"]);

    vendorApp.config(function($routeProvider){
        $routeProvider.when("/",{
            templateUrl : "welcome.html",
            controller : "indexController"
        });
         $routeProvider.when("/login",{
            templateUrl : "login.html",
            controller : "registerLoginController"
        });
        $routeProvider.when("/register",{
            templateUrl : "register.html",
            controller : "registerLoginController"
        });
        $routeProvider.when("/userHome",{
            templateUrl : "user_home.html",
            controller : "userController"
        });
        $routeProvider.when("/vendorHome",{
            templateUrl : "vendor_home.html",
            controller : "vendorController"
        });
        $routeProvider.when("/addProduct",{
            templateUrl : "add_product.html",
            //controller : "vendorController"
        });
        $routeProvider.when("/editProduct",{
            templateUrl : "viewEditProduct.html",
            //controller : "vendorController"
        });
        $routeProvider.when("/admin",{
            templateUrl : "adminPage.html",
            controller : "adminController"
        });
        $routeProvider.otherwise("/",{
            templateUrl : "welcome.html",
            controller : "indexController"
        });
    });
    vendorApp.factory('myService', function(){
		var values = {};
		var savedData = {};
		values.set = function(data){
			savedData = data;
		}
		values.get = function(){
			return savedData;
		}

		return values;
	});