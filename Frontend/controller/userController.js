vendorApp.controller("userController", function ($scope, $http, $location,$route) {
    console.log("start");
    $scope.page = {};
    $scope.page.products = [];
    $scope.cat1 = "";
    $scope.page.showReview = false;

    $scope.search_category = true;    

    $scope.filtering = function(){
        $scope.search_category = !$scope.search_category;
        console.log($scope.cat1);
    }

    $scope.page.getAllProducts = function () {

        var reqJSON = {};
        $http({
            method: "POST",
            url: "http://localhost:8081/product_details_users",
            data: JSON.stringify($scope.page.login)
        }).success(function (respData) {
            console.log(JSON.stringify(respData));
            console.log("Console data " + respData.statusMessage);
            $scope.page.products = respData.products;
        }).error(function (respData) {
            console.log(JSON.stringify(respData));
            $scope.page.login.errorMessage = respData.statusMessage;
            console.log("Console data " + respData.statusMessage);

        });
    };


    $scope.page.getAllProducts();
    $scope.page.buy = function (obj) {

        if (obj.stock<1){
            $.notify("Product Stock not available", "Warn",{position : "right middle"});

        } else {

            console.log("Product buy called" + JSON.stringify(obj));
            var reqJSON = obj;
            $http({
                method: "POST",
                url: "http://localhost:8081/buy_product",
                data: JSON.stringify(reqJSON)
            }).success(function (respData) {
                $.notify("Product Purchased !!!", "success",{position : "right middle"});
                console.log("Success");
            }).error(function (respData) {
                console.log("failure");
            });
        }
    };
    $scope.page.submitReview = function (obj) {
        $scope.page.showReview = false;
        var reqJSON = obj;
        reqJSON.review=Number(reqJSON.review);
        console.log("PsubmitReview called" + JSON.stringify(reqJSON));
        $http({
            method: "POST",
            url: "http://localhost:8081/review_product",
            data: JSON.stringify(reqJSON)
        }).success(function (respData) {
            //console.log(JSON.stringify(respData));
            // console.log("Console data " + respData.statusMessage);
            console.log("Success");
            $route.reload();
        }).error(function (respData) {
            // console.log(JSON.stringify(respData));
            // $scope.page.login.errorMessage = respData.statusMessage;
            // console.log("Console data " + respData.statusMessage);
            console.log("failure");
        });
    };
});