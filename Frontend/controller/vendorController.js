vendorApp.controller("vendorController", function ($scope, $http, $location,myService,$route) {
    console.log("I am starting now");
    
    $scope.page = {};
    $scope.page.register = {};
    $scope.page.register.product = {};
    $scope.page.register.product.specifications = [];
    $scope.page.register.tempSpec={};

        $scope.page.showError = false;
		$scope.page.showmsg = false;
        $scope.page.register.tempSpecShow=false;
        $scope.page.showVendorActions=true;
       var userObj =  myService.get();
       console.log('userObj' + JSON.stringify(userObj));
        $scope.page.showVendorActions=userObj.isActive;
    $scope.page.showerror=!(userObj.isActive);
    $scope.file_changed = function(element) {

     $scope.$apply(function(scope) {
         var photofile = element.files[0];
         var reader = new FileReader();
         reader.onload = function(e) {
            console.log("Image loading");
         };
         reader.readAsDataURL(photofile);
     });
};

$scope.update = function(request){
    console.log(request);
    var data = {
        id : request,
        name : $scope.prodEditName,
        price : $scope.prodEditPrice,
        stock : $scope.prodEditStock,
        category : $scope.prodEditCategory
    } 
     $http({
                method : "POST",
                url : "http://localhost:8081/update",
                data : data
            }).success(function(respData){
                    console.log("success");
                    $route.reload();
                    $.notify("Updated Successfully", "success",{position : "right middle"});
            }).error(function(respData){    
                    console.log("failure");
            });
}

$scope.edit = function(request){
    $scope.prodEditId = request.id;
    $scope.prodEditName = request.name;
    $scope.prodEditPrice = request.price;
    $scope.prodEditStock = request.stock;
    $scope.prodEditCategory = request.category;
}

$scope.remove = function(request){
    console.log(request);
     $http({
                method : "POST",
                url : "http://localhost:8081/remove",
                data : JSON.stringify(request)
            }).success(function(respData){
                    console.log("success");
                    $route.reload();
                    $.notify("Removed Successfully", "success",{position : "right middle"});
            }).error(function(respData){    
                    console.log("failure");
            });
}


$scope.logout = function(){
    $location.path("/login");
}

$scope.page.register.addSpec = function () {
    $scope.page.register.product.specifications.push($scope.page.register.tempSpec);
        $scope.page.register.tempSpec={};
             $scope.page.register.tempSpecShow=false;
             console.log($scope.page.register.product.specifications);
};
$scope.page.register.removeSpec = function(obj){console.log('In' + JSON.stringify(obj));
			var tmp = [];
			for(var i=0;i<$scope.page.register.product.specifications.length;i++){
				var spec = $scope.page.register.product.specifications[i];
                console.log('comp' + JSON.stringify(spec));
					if(spec.specName!=obj.specName && spec.specValue!=obj.specValue){
						tmp.push(spec);
					}
			}
                            console.log('tmp' + JSON.stringify(tmp));
			$scope.page.register.product.specifications=tmp;
		};
$scope.page.register.showTempSpec = function () {
        $scope.page.register.tempSpecShow=true;
        $scope.page.register.tempSpec={};

};
    $scope.page.register.addProduct = function () {
        console.log("Product Function called" + JSON.stringify($scope.page.register.product));
        var reqJSON = $scope.page.register.product;
        $http({
            method: "POST",
            url: "http://localhost:8081/add_product",
            data: JSON.stringify($scope.page.register.product)
        }).success(function (respData) {
    
            console.log("Success");
            $location.path("/vendorHome");
        }).error(function (respData) {

            console.log("failure");
        });
    }
    $scope.page.getAllProducts = function(){
            
            var reqJSON=$scope.page.login;
            $http({
                method : "POST",
                url : "http://localhost:8081/product_details",
                data : JSON.stringify($scope.page.login)
            }).success(function(respData){
                console.log(JSON.stringify(respData));
                console.log("Console data "+respData.statusMessage);
                if(respData.statusMessage=="success"){
                    $scope.page.products=respData.products;
                }
            }).error(function(respData){
                console.log(JSON.stringify(respData));
                $scope.page.login.errorMessage =  respData.statusMessage;
                console.log("Console data "+respData.statusMessage);

            });
        }
$scope.page.getAllProducts();

 $scope.page.gotoAddProduct = function () {
         $location.path("/addProduct");
};
$scope.page.gotoEditProduct = function () {
        $location.path("/editProduct");
};
$scope.page.gotoVendorHome = function () {
        $location.path("/vendorHome");
};

});

