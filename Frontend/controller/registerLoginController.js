vendorApp.controller("registerLoginController",function($scope,$http,$location,myService){
        console.log("start Register controller");
        $scope.page = {};
        $scope.page.login = {};
        $scope.page.register = {};
        $scope.page.register.form = {};
        $scope.page.register.showRegForm = true;
        $scope.page.register.showRegResult = false;
        $scope.page.register.result="";
         $scope.page.register.checkus = function(){
             console.log("Blur Function called!!!");

             var arr = {
              email :   $scope.page.register.form.email
             }
             console.log("Console data "+JSON.stringify(arr));
             $http({
                method : "POST",
                url : "http://localhost:8081/login",
                data : JSON.stringify(arr)
            }).success(function(data){
               $.notify("Valid Email Address", "success",{position : "right middle"});
                console.log("valid email");
            }).error(function(data){
                  $.notify("Existing/Invalid Email Address ", "error",{position : "right middle"});
                console.log("not valid email");
            });
         }

        $scope.page.register.confirm = function(){
            console.log("Request data: "+JSON.stringify($scope.page.register.form));
           
            var reqJSON=$scope.page.register.form;
            $http({
                method : "POST",
                url : "http://localhost:8081/add_user",
                data : JSON.stringify(reqJSON)
            }).success(function(respData){
                $location.path("/login");
                 $.notify("Registered Successfully", "success",{position : "right middle"});
                console.log(JSON.stringify(respData));
                 $scope.page.register.showRegForm = false;
                $scope.page.register.showRegResult = true;
                $scope.page.register.result =  respData.statusMessage;
                console.log("Console data "+respData.statusMessage);

            }).error(function(respData){
                 console.log(JSON.stringify(respData));
               $scope.page.register.showRegForm = false;
                $scope.page.register.showRegResult = true;
                $scope.page.register.result =  respData.statusMessage;
            });
        }


        $scope.page.login.confirm = function(){
            console.log("Request data: "+JSON.stringify($scope.page.login));
        
            var reqJSON=$scope.page.login;
            $http({
                method : "POST",
                url : "http://localhost:8081/validate_login",
                data : JSON.stringify($scope.page.login)
            }).success(function(respData){
                console.log(JSON.stringify(respData));
                console.log("Console data "+respData.statusMessage);
                if(respData.statusMessage=="failure"){
                    $location.path("/login");
                    $.notify("Username and Password Incorrect ", "error",{position : "right middle"});
                }
                if(respData.statusMessage=="success"){
                    myService.set(respData);
                    if(respData.userType=="user"){
                         $location.path("/userHome");
                    }else if(respData.userType=="vendor"){
                         $location.path("/vendorHome");
                    }else if(respData.userType=="admin"){
                        $location.path("/admin");
                    }
                
                }
            }).error(function(respData){
                $location.path("/login");
                 $.notify("Invalid Username and Password", "error",{position : "right middle"});
                $scope.page.login.errorMessage =  respData.statusMessage;
                console.log("Console data "+respData.statusMessage);

            });
        }

       

        $scope.validate = function(){
            if($scope.page.register.form.pass == $scope.cpass){
                console.log("Password matching");
                 $.notify("Password Matching", "success",{position : "right middle"});
            }else{
                if($scope.page.register.form.pass.length == $scope.cpass.length){
                    $.notify("Password Not Matching", "error",{position : "right middle"});
                }
            }
        }

    });