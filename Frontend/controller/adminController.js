vendorApp.controller("adminController", function ($scope, $http, $location, $route) {
    console.log("start");
    $scope.page = {};
    $scope.page.vendors = [];
    $scope.page.allVendors = [];

    $scope.homepage = function () {
        $location.path("/");
    }

    $scope.page.getAllVendors = function () {

        var reqJSON = {};
        $http({
            method: "POST",
            url: "http://localhost:8081/get_all_vendors",
            data: JSON.stringify($scope.page.login)
        }).success(function (respData) {
            console.log(JSON.stringify(respData));
            console.log("Console data " + respData.statusMessage);
            var temp = [];
            var allTemp = [];
            for (var i = 0; i < respData.vendors.length; i++) {
                console.log('respData[i]' + respData[i])
                if (!respData.vendors[i].isActive) {
                    temp.push(respData.vendors[i]);
                }
                allTemp.push(respData.vendors[i]);
            }
            $scope.page.vendors = temp;
            $scope.page.allVendors = allTemp;
            // if (respData.statusMessage == "success") {
            //     $scope.page.products = respData.products;
            // }
        }).error(function (respData) {
            console.log(JSON.stringify(respData));
            $scope.page.login.errorMessage = respData.statusMessage;
            console.log("Console data " + respData.statusMessage);

        });
    }

    $scope.page.getAllVendors();

    $scope.page.approveUser = function (obj) {
        console.log("Approve user called" + JSON.stringify(obj));
        var reqJSON = obj;
        $http({
            method: "POST",
            url: "http://localhost:8081/approve_user",
            data: JSON.stringify(reqJSON)
        }).success(function (respData) {
            console.log("Success");
            $.notify("Vendor approved", "success", { position: "right middle" });
            $route.reload();
        }).error(function (respData) {
            console.log("failure");
        });
    };

      $scope.page.disapproveUser = function (obj) {
        console.log("Dispprove user called" + JSON.stringify(obj));
        var reqJSON = obj;
        $http({
            method: "POST",
            url: "http://localhost:8081/disapprove_user",
            data: JSON.stringify(reqJSON)
        }).success(function (respData) {
            console.log("Success");
            $.notify("Vendor disapproved", "success", { position: "right middle" });
            $route.reload();
        }).error(function (respData) {
            console.log("failure");
        });
    };

});