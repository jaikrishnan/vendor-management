var co = require('co');
const thinky = require('thinky')();
var DBSchema = require('../schema/DBSchema');

class UserService{
    constructor(){
        this.User = DBSchema.User;
    }
    registration(payload){
        const me = this;
        var user = {
            userId : payload.userId,
            mobileNumber : payload.mobileNumber,
            emailId : payload.emailId,
            password : payload.password
        }
        return co(function*(){
                try {
                yield me.User.save(user);
            return {
                    msg : "User Signed Up Successfully"
                }
            } 
            catch (error) {
            }
        }) 
    }
}

module.exports = {
    UserService
}