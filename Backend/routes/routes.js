'use strict'
var joi = require('joi');
var check = require('../check.js');
var dbcheck = require('rethinkdb');
var co = require('co');
var result = [];
var answer;
var thinky = require('thinky')();
var r = thinky.r;
var arr = [];
var routeHandler = require('../handler/routeHandler');

class Routes {
    constructor() {
        this.routeHandler = new routeHandler();
    }
    registerRoutes(server) {
        const me = this;
        server.log("Registering all routes");
        server.route({
            method: 'POST',
            path: '/add_user',
            handler: (request, reply) => me.routeHandler.registration(request, reply)
        })
    }
}

module.exports = Routes;