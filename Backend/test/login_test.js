var chai = require('chai'),
    sinon = require('sinon');

chai.config.includeStack = true;
global.expect = chai.expect;
global.AssertionError = chai.AssertionError;
global.Assertion = chai.Assertion;
global.assert = chai.assert;


var loginPromise = new Promise(
    (resolve, reject) => {
        return resolve([{
            email: "jai@gmail.com"
        }])
    }
);

var vendorPromise = new Promise(
    (resolve, reject) => {
        return resolve([{
            result: true
        }])
    }
)

var updatePromise = new Promise(
    (resolve, reject) => {
        return resolve([{
            result: true
        }])
    }
)

var approveUserPass = new Promise(
    (resolve, reject) => {
        return resolve( true  )
    }
)

var approveUserFail = new Promise(
    (resolve, reject) => {
        return resolve( false  )
    }
)



var disapproveUserPass = new Promise(
    (resolve, reject) => {
        return resolve( true )
    }
)

var disapproveUserFail = new Promise(
    (resolve, reject) => {
        return resolve(false)
    }
)


const helper = require("../Handler.js").Helper;

var h = new helper();
var db_login = require("../DBhandler/login_reg");
var db_admin = require("../DBhandler/db_admin");
var db_vendor = require("../DBhandler/db_vendor");
var spy = sinon.spy();

sinon.stub(db_login, 'login').returns(loginPromise);
sinon.stub(db_admin, 'getAllVendors').returns(vendorPromise);
sinon.stub(db_vendor, 'update').returns(updatePromise);

describe.only('Login', function () {
    it('Login Valid', function () {
        var arr = {
            "email": "jai@gmail.com",
            "pass": "jai"
        }
        var exp = {
            userType: 'vendor',
            statusMessage: 'success',
            userName: 'jai',
            email: 'jai@gmail.com',
            isActive: true

        };
        exp = JSON.stringify(exp);
        var result = h.validatelogin(arr);
        //  console.log(result);
        return Promise.resolve(result)
            .then(function (m) {
                //  console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })

    it('Login invalid', function () {
        var arr = {
            "email": "jai@gmail.com",
            "pass": "asdfasjai"
        }
        var exp = { "statusMessage": "failure" };
        exp = JSON.stringify(exp);
        var result = h.validatelogin(arr);
        return Promise.resolve(result)
            .then(function (m) { expect(JSON.stringify(m)).to.equal(exp); })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })

    it('Login Null', function () {
        var arr = {
            "email": "",
            "pass": ""
        };
        var exp = { "statusMessage": "failure" };
        exp = JSON.stringify(exp);
        var result = h.validatelogin(arr);
        return Promise.resolve(result)
            .then(function (m) { expect(JSON.stringify(m)).to.equal(exp); })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })


})

// Unique username check
describe.only('Username Check', function () {
    it('userID Invalid', function () {
        var arr = {
            "email": "jai@gmail.com"
        }
        var exp = { "statusMessage": "failure" };
        exp = JSON.stringify(exp);
        var result = h.login(arr);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })
})

///// getAllVemdor
describe.only('getAll Vendors', function () {
    it('vendor list', function () {
        var exp = { "statusMessage": "success", "vendors": [{ "result": true }] };
        exp = JSON.stringify(exp);
        var result = h.getAllVendors();
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })
})

describe.only('Update', function () {
    it('Update a Product', function () {
        var req = {
            id: "",
            name: "",
            price: "",
            stock: "",
            category: ""
        }
        var exp = true;
        exp = JSON.stringify(exp);
        var result = h.update(req);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })
})

describe.only('approveUserPass', function () {
    var current;
    current = approveUserPass;

    beforeEach(function () {
        sinon.stub(db_admin, 'approveUser').returns(current);
    })

    afterEach(function () {
        db_admin.approveUser.restore();
        current = approveUserFail;

    })
    it('Approve user Pass', function () {
        var req = {
            id: "jksdajfkdjaqwerjknsadfnkl"
        }
        var exp = true;
        exp = JSON.stringify(exp);
        var result = h.approveUser(req);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })


    it('Approve user Fail', function () {
        var req = {
            id: "jksdajfkdjaqwerjknsadfnkl"
        }
        var exp = false;
        exp = JSON.stringify(exp);
        var result = h.approveUser(req);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })
})

describe.only('disapproveUserPass', function () {
    var current;
    current = disapproveUserPass;

    beforeEach(function () {
        sinon.stub(db_admin, 'disapproveUser').returns(current);
    })

    afterEach(function () {
        db_admin.disapproveUser.restore();
        current = disapproveUserFail;

    })
    it('Disapprove user Pass', function () {
        var req = {
            id: "jksdajfkdjaqwerjknsadfnkl"
        }
        var exp = true;
        exp = JSON.stringify(exp);
        var result = h.disapproveUser(req);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })


    it('Dispprove user Fail', function () {
        var req = {
            id: "jksdajfkdjaqwerjknsadfnkl"
        }
        var exp = false;
        exp = JSON.stringify(exp);
        spy(db_admin,'disapproveUser');
        var result = h.disapproveUser(req);
        return Promise.resolve(result)
            .then(function (m) {
                console.log('//////');
                console.log(m);
                console.log("Arguments 1");
                console.log(spy.firstCall.args);
                console.log("Arguments 2");
                console.log(spy.printf);
                console.log("Cal count of Dispprove user");
                console.log(spy.callCount);
                expect(JSON.stringify(m)).to.equal(exp);
            })
            .catch(function (m) { console.log(m); throw new Error("Fail"); })
    })
})













//     it('userID Null', function () {
//         var arr = {
//             "email": ""
//         }
//         var exp = { "statusMessage": "NULL" };
//         exp = JSON.stringify(exp);
//         var result = h.login(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

//     it('userID valid', function () {
//         var arr = {
//             "email": "jai@gmailyahoo.com"
//         }
//         var exp = { "statusMessage": "success" };
//         exp = JSON.stringify(exp);
//         var result = h.login(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

// })


// describe.only('User Registration', function () {
//     it('Vendor Not selected', function () {
//         var arr = { "username": "samsung",
//          "email": "sam@samsung.com", 
//          "gender": "male", 
//          "phone": "1234567890", 
//          "userType": "", 
//          "address": "chennai", 
//          "pass": "jai" }

//          var exp = "failure";
//         exp = JSON.stringify(exp);
//         var result = h.addUser(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m.statusMessage)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })

//     })

//     it('Phone Number Not valid', function () {
//         var arr = { "username": "samsung",
//          "email": "sam@samsung.com", 
//          "gender": "male", 
//          "phone": "1234567890fasdf", 
//          "userType": "vendor", 
//          "address": "chennai", 
//          "pass": "jai" }

//          var exp = "failure";
//         exp = JSON.stringify(exp);
//         var result = h.addUser(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m.statusMessage)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

//     it('valid Vendor', function () {
//         var arr = { "username": "samsung",
//          "email": "sam@skjkamsung1.com", 
//          "gender": "male", 
//          "phone": "1234567890", 
//          "userType": "vendor", 
//          "address": "chennai", 
//          "pass": "jai" }

//          var exp = "success";
//         exp = JSON.stringify(exp);
//         var result = h.addUser(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m.statusMessage)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

//     it('valid User', function () {
//         var arr = { "username": "samsung",
//          "email": "sam@samsnkjn1ung.com", 
//          "gender": "male", 
//          "phone": "1234567890", 
//          "userType": "user", 
//          "address": "chennai", 
//          "pass": "jai" }

//          var exp = "success";
//         exp = JSON.stringify(exp);
//         var result = h.addUser(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m.statusMessage)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

//      it('Existing User with same ID', function () {
//         var arr = { "username": "samsung",
//          "email": "jai@gmail.com", 
//          "gender": "male", 
//          "phone": "1234567890", 
//          "userType": "user", 
//          "address": "chennai", 
//          "pass": "jai" }

//          var exp = "failure";
//         exp = JSON.stringify(exp);
//         var result = h.addUser(arr);
//         return Promise.resolve(result)
//             .then(function (m) { expect(JSON.stringify(m.statusMessage)).to.equal(exp); })
//             .catch(function (m) { console.log(m); throw new Error("Fail"); })
//     })

// })
