'use strict'
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var thinky = require('thinky')();
var cookieParser = require("cookie-parser");
var r = thinky.r;
var co = require("co");
var db_login = require("./DBhandler/login_reg");
var db_vendor = require("./DBhandler/db_vendor");
var db_admin = require("./DBhandler/db_admin");
var db_user = require("./DBhandler/db_user");

class Helper {
    constructor() {

    }
    handleError(res) {
        return function (error) {
            res.send(500, { error: error.message });
        }
    }


    login(req) {
        let me = this;
        return co(function* () {
            var JSONResp = {};
            if (req.email == "") {
                JSONResp.statusMessage = "NULL";
                return JSONResp;
            }

            var result = yield db_login.login();
            if (result == -1) {
                JSONResp.statusMessage = "NULL";
                return JSONResp;
            }

            var flag = true;
            for (var i = 0; i < result.length; i++) {
                if (result[i].email == req.email) {
                    flag = false;
                    JSONResp.statusMessage = "failure";
                    break;
                }
            }
            if (flag) {
                JSONResp.statusMessage = "success";
            }

            return JSONResp;
        })
    }


    addUser(req) {
        var JSONrespfailure = {};
        JSONrespfailure.statusMessage = "failure";
        JSONrespfailure.statusCode = 401;

        var JSONresp = {};
        JSONresp.statusMessage = "success";
        JSONresp.statusCode = 200;

        if (req.userType == 'vendor') {
            req.isActive = false;
        }
        if (req.userType == "") {
            return JSONrespfailure;
        }
        if (req.phone.length != 10) {
            return JSONrespfailure;
        }

        return this.login(req).then(function (result) {
            if (result.statusMessage == "failure") {
                return JSONrespfailure;
            } else {
                return co(function* () {
                    var result = yield db_login.addUser(req);
                    console.log(req);


                    if (result.inserted !== 1) {
                        return JSONrespfailure;
                    }
                    else {
                        return JSONresp;
                    }

                })
            }
        })

    }



    validatelogin(req) {
        console.log(req);

        return co(function* () {
            var result = yield db_login.validatelogin(req);
            if (!result)
                return new Error("wrong call");
            var JSONResp = {};

            if (result && result.length > 0) {
                JSONResp.userType = result[0].userType;
                JSONResp.statusMessage = "success";
                JSONResp.userName = result[0].username;
                JSONResp.email = result[0].email;
                if (JSONResp.userType == 'vendor') {
                    JSONResp.isActive = result[0].isActive;
                }

            } else {
                JSONResp.statusMessage = "failure";
            }

            return JSONResp;


        })
    }

    addProduct(req) {
        return co(function* () {
            var reqJSON = req.body;
            reqJSON.reviews = [];
            reqJSON.avg = 0;
            reqJSON.vendor = req.cookies.email;
            var result = yield db_vendor.addProduct(reqJSON);
            if (result) {
                return true;
            } else {
                return false;
            }

        })

    }

    productDetailsGet(req, res, next) {
        return co(function* () {
            var result = yield db_vendor.productDetailsGet();
            if (result) {
                res.send(JSON.stringify(result))
            }
        })
    }

    productDetailsPost(req, res, next) {
        console.log("Cookie value for product details is " + req.cookies.email);
        console.log("Got a POST request for product_details --> " + JSON.stringify(req.body));

        return co(function* () {
            var result = yield db_vendor.productDetailsPost(req);
            if (result) {
                var JSONResp = {};
                JSONResp.statusMessage = "success";
                JSONResp.products = result;
                res.status(200).send(JSON.stringify(JSONResp));
            } else {
                JSONResp.statusMessage = "failure";
                res.status(500).send(JSON.stringify(JSONResp));
            }
        })
    }

    productDetailsUsers(req) {
        console.log("/product_details_users");
        var JSONResp = {};
        var products = [];
        return co(function* () {
            var result = yield db_vendor.productDetailsUsers();
            if (result) {
                return result;
            } else {
                return false;
            }
        })
    }

    buyProduct(req) {
        return co(function* () {
            var result = yield db_user.buyProduct(req.body.id);
            if (!result) {
                return false;
            } else {
                return true;
            }
        })
    }

    reviewProduct(req) {
        var reqJSON = {};
        var avg = 0;
        reqJSON.email = String(req.cookies.email);
        reqJSON.review = req.body.review;
        var arr = {
            email: reqJSON.email,
            review: reqJSON.review,
            id: req.body.id
        }
        return co(function* () {
            var result = yield db_user.reviewProduct(arr);

            if (result1) {
                return true;
            } else {
                return false;
            }
        });
    }

    ///testing
    getAllVendors() {
        var JSONResp = {};
        return co(function* () {
            var result = yield db_admin.getAllVendors();
            if (result) {
                JSONResp.statusMessage = "success";
                JSONResp.vendors = result;
            } else {
                JSONResp.statusMessage = "failure";
            }
            return JSONResp;
        })
    }

    approveUser(req) {
        var JSONResp = {};
        return co(function* () {
            var result = yield db_admin.approveUser(req);
            if (result) {
                return true;
            } else {
                return false;
            }
        })
    }

    disapproveUser(req) {
        var JSONResp = {};
        return co(function* () {
            var result = yield db_admin.disapproveUser(req);
            if (result) {
                return true;
            } else {
                return false;
            }
        })
    }

    getReviewData(req, res, next) {
        var reqJSON = req.body;
        console.log("Got a POST request for get detail  product Review" + JSON.stringify(req.body));
        r.db('vendor').table('product_review').filter({ name: reqJSON.name }).
            filter({ vendor: reqJSON.vendor }).getField('review').avg().then(function (result) {
                var JSONresp = {};
                console.log("success in getting:" + JSON.stringify(result));
                res.send(JSON.stringify(result));
            }).error(new Error("wrong call"));
    }

    update(req, res) {
        var JSONResp = {};
        return co(function* () {
            var result = yield db_vendor.update(req);
            if (result) {
                return true;
            } else {
                return false;
            }
        })
    }

    remove(req, res) {
        var JSONResp = {};
        return co(function* () {
            var result = yield db_vendor.remove(req);
            if (result) {
                JSONResp.statusMessage = "success";
                res.status(200).send(JSON.stringify(JSONResp));
            } else {
                JSONResp.statusMessage = "failure";
                res.status(500).send(JSON.stringify(JSONResp));
            }
        })
    };
}

module.exports = {
    Helper: Helper
}