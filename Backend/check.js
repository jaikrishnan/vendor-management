var r = require('rethinkdb');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var app = require('./index').app;
var Helper = require('./Handler').Helper;
var helper = new Helper();
var co = require("co");

app.post('/add_user', jsonParser, function (req, res, next) {
    return co(function* () {
        var result = yield helper.addUser(req.body);
        if (result.statusMessage == "success") {
            res.status(200).send("success");
        } else {
            res.status(401).send("failure");
        }

    });
});

app.post('/login', jsonParser, function (req, res, next) {

    return co(function* () {
        var result = yield helper.login(req.body);
        if (result.statusMessage == "success") {
            res.status(200).send("success");
        } else if (result.statusMessage == "NULL") {
            res.status(500).send("NULL");
        } else {
            res.status(401).send("failure");
        }

    });


});

app.post('/validate_login', jsonParser, function (req, res, next) {
    //use yield
    return co(function* () {
        var result = yield helper.validatelogin(req.body);

        if (result.statusMessage == "success") {
            console.log("Adding cookie email " + req.body.email);
            res.cookie("email", req.body.email).status(200).send(JSON.stringify(result));
        } else {
            res.status(401).send(JSON.stringify(result));
        }

    });

});


app.post('/add_product', jsonParser, function (req,res) {
    return co(function* () {
        var result = yield helper.addProduct(req);

        var JSONresp = {};
        
        if(result){
            JSONresp.statusMessage = "success";
            res.status(200).send(JSONresp);
        }else{
            JSONresp.statusMessage = "failure";
            res.status(500).send(JSONresp);
        }
    })

});

app.get('/product_details', jsonParser, function (req, res, next) {
    helper.productDetailsGet(req, res, next);
});

app.post('/product_details', jsonParser, function (req, res, next) {
    helper.productDetailsPost(req, res, next);
});

app.post('/product_details_users', jsonParser, function (req, res) {
    return co(function* () {
        var JSONresp = {};
        var result = yield helper.productDetailsUsers(req);
        if (result) {
            JSONresp.statusMessage = "success";
            JSONresp.products = result;
            res.status(200).send(JSON.stringify(JSONresp));
        } else {
            JSONresp.statusMessage = "failure";
            res.status(500).send(JSON.stringify(JSONresp));
        }
    });

})

app.post('/buy_product', jsonParser, function (req, res) {
    return co(function* () {
        var JSONResp = {};
        var result = yield helper.buyProduct(req);
        if (result) {
            JSONResp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONResp));
        } else {
            JSONResp.statusMessage = "failure";
            res.status(500).send(JSON.stringify(JSONResp));
        }
    })
});

app.post('/review_product', jsonParser, function (req, res) {
    var JSONresp = {};

    return co(function* () {
        var result = yield helper.reviewProduct(req);

        if (result) {
            JSONresp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONresp));
        } else {
            JSONresp.statusMessage = "failure";
            res.status(401).send(JSON.stringify(JSONresp));
        }

    })
});

app.post('/get_all_vendors', jsonParser, function (req, res) {
    return co(function* () {
        var result = yield helper.getAllVendors();
        if (result.statusMessage == "success") {
            res.status(200).send(JSON.stringify(result))
        } else {
            res.status(401).send(JSON.stringify(result));
        }
    })
});

app.post('/approve_user', jsonParser, function (req, res) {
    var JSONResp = {};
    return co(function* () {
        var result = yield helper.approveUser(req);
        if (result) {
            JSONResp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONResp));
        } else {
            JSONResp.statusMessage = "failure";
            res.status(500).send(JSON.stringify(JSONResp));
        }
    })
});


app.post('/disapprove_user', jsonParser, function (req, res) {
    var JSONResp = {};
    return co(function* () {
        var result = yield helper.disapproveUser(req);
        if (result) {
            JSONResp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONResp));
        } else {
            JSONResp.statusMessage = "failure";
            res.status(500).send(JSON.stringify(JSONResp));
        }
    })

});


app.post('/get_review_data', jsonParser, function (req, res) {
    return co(function*(){
        var JSONresp = {};
        var result = yield helper.getReviewData(req);
        if(result){
            JSONresp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONresp));
        }else{
            JSONresp.statusMessage = "failure";
            res.status(401).send(JSON.stringify(JSONresp))
        }
    })
    
});

app.post('/update', jsonParser, function (req, res) {
    return co(function*(){
        var JSONresp = {};
        var result = yield helper.update(req, res);
        if(result){
            JSONresp.statusMessage = "success";
            res.status(200).send(JSON.stringify(JSONresp));
        }else{
            JSONresp.statusMessage = "failure";
            res.status(401).send(JSON.stringify(JSONresp))
        }
    })
    
});

app.post('/remove', jsonParser, function (req, res) {
    helper.remove(req, res);
});


