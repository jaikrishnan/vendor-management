'use strict'
var express = require('express');
var app = express();
var thinky = require('thinky')();
var r = thinky.r;
var bodyParser = require('body-parser');
var cookieParser = require("cookie-parser");
app.use(cookieParser());
app.use(express.static('/Users/ecom-jai.krishnan/Documents/project/Frontend'));
app.use('/static', express.static('/Users/ecom-jai.krishnan/Documents/project/Frontend'));
var jsonParser = bodyParser.json();
var data = {};
var r = require('rethinkdb');
app.use(createConnection);
function handleError(res) {
    return function (error) {
        res.send(500, { error: error.message });
    }
}

function createConnection(req, res, next) {
    var dbconfig = {
        host: "127.0.0.1",
        port: 28015,
        db: "vendor"
    }
    r.connect(dbconfig).then(function (conn) {
        req._rdbConn = conn;
        next();
    }).error(handleError(res));
}

function conn() {
    app.use(createConnection).then(function () {
        return con;
    })

}

var server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})


module.exports = {
    app: app
}