'use strict'
var thinky = require('thinky')();
var r = thinky.r;
var co = require("co");

class LoginAccess{
    constructor(){

    }
    login(){
        return co(function* (){

            try{
            var result = yield r.db('vendor').table('user_details').pluck('email');
            if(!result){
                return -1;
            }else{
                return result;
            }
            }
            catch(err){
                    throw err;
            }
            
        })
    }
    addUser(req){
        return co(function* (){
            try{
                var result = yield r.db('vendor').table('user_details').insert(req, {returnChanges: true});
                if(!result){
                    return -1;
                }else{
                    return result;
                }
            }catch(err){
                throw err;
            }
        })
    }
    validatelogin(req){
        return co(function* (){
            try{
                var result = yield r.db('vendor').table('user_details').filter({ 'email': req.email, 'pass': req.pass });
                if(!result){
                    return -1;
                }else{
                    return result;
                }
            }catch(err){
                throw err;
            }
        })
    }
}

var db = new LoginAccess();
module.exports = db;