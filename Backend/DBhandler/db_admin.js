'use strict'
var thinky = require('thinky')();
var r = thinky.r;
var co = require("co");

class AdminAccess {
    constructor() {

    }
    approveUser(req) {
        return co(function* () {
            var result = yield r.db('vendor').table("user_details").filter({ id: req.body.id }).update({ 'isActive': true });
            if (!result) {
                return false;
            } else {
                return true;
            }
        })
    }

    disapproveUser(req) {
        return co(function* () {
            var result = yield r.db('vendor').table("user_details").filter({ id: req.body.id }).update({ 'isActive': false });
            if (!result) {
                return false;
            } else {
                return true;
            }
        })
    }

    getAllVendors(){
        return co(function* (){
            var result = yield r.db('vendor').table("user_details").filter({ userType: 'vendor' });
            if(!result){
                return false;
            }else{
                return result;
            }
        })
    }
}

var db_admin = new AdminAccess();
module.exports = db_admin;