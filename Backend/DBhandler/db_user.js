'use strict'
var thinky = require('thinky')();
var r = thinky.r;
var co = require("co");

class UserAccess {
    constructor() {

    }

    buyProduct(pid) {
        var stockVal = 0;
        return co(function* () {
            var result1 = yield r.db('vendor').table('product_details').filter({ id: pid });
            if (!result1) {
                return false;
            } else {
                stockVal = parseInt(result1[0].stock);
                stockVal = stockVal - 1;
                var result2 = yield r.db('vendor').table('product_details').filter({ id: pid }).update({ stock: stockVal });
                if (!result2) {
                    return false;
                } else {
                    return true;
                }
            }
        })
    }

    reviewProduct(req) {
        return co(function* () {
            console.log(req);
            var result1 = yield r.db('vendor').table('product_details').update({
                reviews: r.row('reviews').append({
                    "email": req.email,
                    "rating": req.review
                })
            });
            if (!result1) {
                return false;
            } else {
                var result2 = yield r.db('vendor').table('product_details').get(req.id).update({
                    avg: r.row('reviews').avg('rating')
                });
                if(result2){
                    return true;
                }else{
                    return false;
                }
            }
        });
    }

    getReivewData(){
        
    }
}

var db_userAccess = new UserAccess();
module.exports = db_userAccess;