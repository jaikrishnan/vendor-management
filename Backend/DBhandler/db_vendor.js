'use strict'
var thinky = require('thinky')();
var r = thinky.r;
var co = require("co");

class VendorAccess {
    constructor() {

    }

    addProduct(req) {
        return co(function* () {
            try {
                console.log(req);
                var result = yield r.db('vendor').table('product_details').insert(req, { returnChanges: true });
                if (!result) {
                    return false;
                } else {
                    return true;
                }
            } catch (err) {
                throw err;
            }
        })
    }

    productDetailsGet() {
        return co(function* () {
            try {
                var result = yield r.db('vendor').table('product_details');
                if (!result) {
                    return false;
                } else {
                    result.toArray().then(function (data) {
                        return data;
                    })
                }
            } catch (err) {
                throw err;
            }
        })
    }

    productDetailsPost(req) {
        return co(function* () {
            var result = r.db('vendor').table('product_details').filter({ 'vendor': req.cookies.email });
            if (!result) {
                return false;
            } else {
                return result;
            }
        })
    }

    productDetailsUsers() {
        return co(function* () {
            var result = yield r.db('vendor').table('product_details');
            if (!result) {
                return false;
            } else {
                return result;
            }
        })
    }

    update(req) {
        return co(function* () {
            var result = yield r.db('vendor').table('product_details').filter({ id: req.body.id }).update({ 'name': req.body.name, "price": req.body.price, "stock": req.body.stock, "category": req.body.category });
            if (!result) {
                return false;
            } else {
                return true;
            }
        })
    }

    remove(req) {
        return co(function* () {
            var result = r.db('vendor').table('product_details').get(req.body.id).delete();
            if (!result) {
                return false;
            } else {
                return true;
            }
        })
    }

}

var db_vendor = new VendorAccess();
module.exports = db_vendor;