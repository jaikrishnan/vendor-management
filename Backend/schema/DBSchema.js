'use strict'
var thinky = require('thinky')();
var type = thinky.type;


var User = thinky.createModel("user", {
    userId: type.string().required(),
    mobileNumber: type.number().min(7000000000).max(9999999999).required(),
    emailId:type.string().email().required(),
    password:type.string().required()
});

module.exports = {
    User 
}